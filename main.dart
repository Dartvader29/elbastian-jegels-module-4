import 'package:flutter/material.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:flutter/cupertino.dart';

void main() => runApp(const SignUpApp());

class SignUpApp extends StatelessWidget {
  const SignUpApp();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'EJ App',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      debugShowCheckedModeBanner: false,
      routes: {
        '/': (context) => const SignUpScreen(),
      },
    );
  }
}

class SignUpScreen extends StatelessWidget {
  const SignUpScreen();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[200],
      body: const Center(
        child: SizedBox(
          width: 400,
          child: Card(
            child: SignUpForm(),
          ),
        ),
      ),
    );
  }
}

class DashBoard extends StatelessWidget {
  const DashBoard({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: Column(children: [
          ///child one
          TextButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return const FeatureScreenUno(title: 'Feature Screen uno');
                }));
              },
              child: const Text('Feature Screen uno')),

          ///child two
          TextButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const FeatureScreenDos(title: 'Feature Screen dos');
              }));
            },
            child: const Text('Feature Screen dos'),
          ),
          TextButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const UserProfile(title: 'User Profile Edit');
              }));
            },
            child: const Text('User Profile Edit'),
          ),
        ]));
  }
}

class FeatureScreenUno extends StatelessWidget {
  const FeatureScreenUno({Key? key, required String title});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Feature Screen uno',
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Feature Screen uno'),
          ),
          body: const Center(
            child: Text('Screen one'),
          ),
        ));
  }
}

class FeatureScreenDos extends StatelessWidget {
  const FeatureScreenDos({Key? key, required String title});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Feature Screen dos',
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Feature Screen dos'),
          ),
          body: const Center(
            child: Text('Screen two'),
          ),
        ));
  }
}

class UserProfile extends StatelessWidget {
  const UserProfile({Key? key, required String title});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'User Profile Edit',
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: const Text('User Profile Edit'),
          ),
          body: Center(
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'First Name',
                    ),
                  ),
                  TextField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Surname',
                    ),
                  ),
                  TextField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Username',
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      final snackBar = const SnackBar(
                        content: const Text('Saved successfully'),
                      );
                    },
                    child: const Text('Save'),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}

class SignUpForm extends StatefulWidget {
  const SignUpForm();

  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final _firstNameTextController = TextEditingController();
  final _lastNameTextController = TextEditingController();
  final _usernameTextController = TextEditingController();

  final double _formProgress = 0;

  void _showDashBoard() {
    Navigator.of(context).pushNamed('Dashboard');
  }

  void _showFeatureScreenUno() {
    Navigator.of(context).pushNamed('Feature Screen uno');
  }

  void _showFeatureScreenDos() {
    Navigator.of(context).pushNamed('Feature Screen dos');
  }

  void _showUserProfile() {
    Navigator.of(context).pushNamed('User Edit');
  }

  void showSnackBar() {
    ScaffoldMessenger.of(context).showSnackBar;
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          LinearProgressIndicator(value: _formProgress),
          Text('Log in', style: Theme.of(context).textTheme.headline4),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: _firstNameTextController,
              decoration: const InputDecoration(hintText: 'First name'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: _lastNameTextController,
              decoration: const InputDecoration(hintText: 'Surname'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: _usernameTextController,
              decoration: const InputDecoration(hintText: 'Username'),
            ),
          ),
          TextButton(
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.resolveWith(
                    (Set<MaterialState> states) {
                  return states.contains(MaterialState.disabled)
                      ? null
                      : Colors.white;
                }),
                backgroundColor: MaterialStateProperty.resolveWith(
                    (Set<MaterialState> states) {
                  return states.contains(MaterialState.disabled)
                      ? null
                      : Colors.blue;
                }),
              ),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return const DashBoard(title: 'Dashboard');
                }));
              },
              child: const Text("Log In")),
        ],
      ),
    );
  }
}

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(seconds: 3)).then((value) {
      Navigator.of(context).pushReplacement(CupertinoPageRoute(
          builder: (ctx) => const DashBoard(
                title: '',
              )));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            SpinKitFadingCircle(
              color: Colors.red,
              size: 50.0,
            )
          ],
        ),
      ),
    );
  }
}
